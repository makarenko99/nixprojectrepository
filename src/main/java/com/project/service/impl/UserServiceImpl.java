package com.project.service.impl;

import com.project.repository.RoleRepository;
import com.project.repository.UserRepository;
import com.project.repository.model.Role;
import com.project.repository.model.User;
import com.project.security.jwt.JwtTokenProvider;
import com.project.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestFilter;
import org.springframework.security.web.servletapi.SecurityContextHolderAwareRequestWrapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final static String ROLE_USER = "USER";
    private final static String ROLE_SUPER_ADMIN = "SUPER_ADMIN";

    private static final Logger LOG = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    private final RoleRepository roleRepository;
    private final JwtTokenProvider jwtTokenProvider;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder, RoleRepository roleRepository, JwtTokenProvider jwtTokenProvider) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.roleRepository = roleRepository;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public List<User> findAll() {
        List<User> result = userRepository.findAll();
        LOG.info("IN findAll - {} users found", result.size());
        return result;
    }

    @Override
    public User getById(Long id) {
        User result = userRepository.findById(id).orElse(null);
        if (result == null) {
            LOG.warn("IN getById - no user found by id: {}", id);
            return null;
        }
        LOG.info("IN getById - user: {}", result);
        return result;
    }

    @Override
    public User findByUsername(String name) {
        User result = userRepository.findByUsername(name);
        LOG.info("IN findByName - user: {} found by name: {}", result, name);
        return result;
    }

    @Override
    public User updateUserById(Long id, User user) {
        User updatedUser = null;

        if (jwtTokenProvider.hasRole(ROLE_SUPER_ADMIN)) {
            User oldUserEntity = userRepository.findById(id).orElse(null);

            if (oldUserEntity == null) {
                LOG.warn("IN updateUserById - no user found by id: {}", id);
                return null;
            }

            oldUserEntity.setUsername(user.getUsername());
            oldUserEntity.setPassword(user.getPassword());

            updatedUser = userRepository.save(oldUserEntity);
            LOG.info("IN updateUserById - user: {} successfully updated", updatedUser);
        } else {
            LOG.info("IN updateUserById - user: {} failed updated", updatedUser);
        }
        return updatedUser;
    }

    @Override
    public User create(User user) {
        User createdUser = null;

        if (jwtTokenProvider.hasRole(ROLE_SUPER_ADMIN)) {
            Role roleUser = roleRepository.findByName(ROLE_USER);
            List<Role> userRoles = new ArrayList<>();
            userRoles.add(roleUser);

            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setRoles(userRoles);

            createdUser = userRepository.save(user);

            LOG.info("IN create - user: {} successfully created", createdUser);
        } else {
            LOG.info("IN create - user: {} failed created", createdUser);
        }
        return createdUser;
    }

    @Override
    public void deleteUserById(Long userId) {
        if (jwtTokenProvider.hasRole(ROLE_SUPER_ADMIN)) {
            userRepository.deleteById(userId);
        } else {
            LOG.info("IN delete - user with id: {} failed deleted", userId);
        }
        LOG.info("IN delete - user with id: {} successfully deleted", userId);
    }

}
