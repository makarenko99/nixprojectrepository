package com.project.service.impl;

import com.project.repository.model.Role;
import com.project.repository.RoleRepository;
import com.project.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private static final Logger log = LoggerFactory.getLogger(RoleServiceImpl.class);
    private final RoleRepository roleRepository;

    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> findAll() {
        List<Role> result = roleRepository.findAll();
        log.info("IN findAll - {} roles found", result.size());
        return result;
    }

    @Override
    public Role getById(Long id) {
        Role result = roleRepository.findById(id).orElse(null);
        if (result == null) {
            log.warn("IN getById - no role found by id: {}", id);
            return null;
        }
        log.info("IN getById - role: {}", result);
        return result;
    }

    @Override
    public Role updateRoleById(Long id, Role role) {
        Role oldRoleEntity = roleRepository.findById(id).orElse(null);

        if (oldRoleEntity == null) {
            log.warn("IN updateRoleById - no role found by id: {}", id);
            return null;
        }

        oldRoleEntity.setName(role.getName());

        Role updatedRole = roleRepository.save(oldRoleEntity);
        log.info("IN updateRoleById - role: {} successfully updated", updatedRole);
        return updatedRole;
    }

    @Override
    public Role create(Role role) {
        Role createdRole = roleRepository.save(role);
        log.info("IN create - role: {} successfully created", createdRole);
        return createdRole;
    }

    @Override
    public void deleteRoleById(Long userId) {
        roleRepository.deleteById(userId);
        log.info("IN delete - role with id: {} successfully deleted", userId);
    }
}
