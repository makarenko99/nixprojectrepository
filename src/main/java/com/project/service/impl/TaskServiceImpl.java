package com.project.service.impl;

import com.project.repository.model.Task;
import com.project.repository.TaskRepository;
import com.project.security.jwt.JwtTokenProvider;
import com.project.service.TaskService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskServiceImpl implements TaskService {

    private final static String ROLE_ADMIN = "ADMIN";

    private static final Logger log = LoggerFactory.getLogger(TaskServiceImpl.class);
    private final TaskRepository taskRepository;
    private final JwtTokenProvider jwtTokenProvider;

    public TaskServiceImpl(TaskRepository taskRepository, JwtTokenProvider jwtTokenProvider) {
        this.taskRepository = taskRepository;
        this.jwtTokenProvider = jwtTokenProvider;
    }

    @Override
    public List<Task> findAll() {
        List<Task> result = taskRepository.findAll();
        log.info("IN findAll - {} tasks found", result.size());
        return result;
    }

    @Override
    public Task getById(Long id) {
        Task result = taskRepository.findById(id).orElse(null);
        if (result == null) {
            log.warn("IN getById - no task found by id: {}", id);
            return null;
        }
        log.info("IN getById - task: {}", result);
        return result;
    }

    @Override
    public Task updateTaskById(Long id, Task task) {
        Task updatedTask = null;

        if (jwtTokenProvider.hasRole(ROLE_ADMIN)) {
            Task oldTaskEntity = taskRepository.findById(id).orElse(null);

            if (oldTaskEntity == null) {
                log.warn("IN updateTaskById - no task found by id: {}", id);
                return null;
            }

            oldTaskEntity.setName(task.getName());

            updatedTask = taskRepository.save(oldTaskEntity);
            log.info("IN updateTaskById - user: {} successfully updated", updatedTask);
        } else {
            log.info("IN updateTaskById - user: {} failed updated", updatedTask);
        }
        return updatedTask;
    }

    @Override
    public Task create(Task task) {
        Task createdTask = null;
        if (jwtTokenProvider.hasRole(ROLE_ADMIN)) {
            createdTask = taskRepository.save(task);
            log.info("IN create - task: {} successfully created", createdTask);
        } else {
            log.info("IN create - task: {} failed created", createdTask);
        }
        return createdTask;
    }

    @Override
    public void deleteTaskById(Long userId) {
        taskRepository.deleteById(userId);
        log.info("IN delete - task with id: {} successfully deleted", userId);
    }
}
