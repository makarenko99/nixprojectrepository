package com.project.service;

import com.project.repository.model.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    User getById(Long id);

    User findByUsername(String name);

    User updateUserById(Long id, User user);

    User create(User user);

    void deleteUserById(Long id);
}
