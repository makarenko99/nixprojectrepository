package com.project.service;

import com.project.repository.model.Role;

import java.util.List;

public interface RoleService {
    List<Role> findAll();

    Role getById(Long id);

    Role updateRoleById(Long id, Role role);

    Role create(Role role);

    void deleteRoleById(Long id);
}
