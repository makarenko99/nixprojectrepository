package com.project.service;

import com.project.repository.model.Task;

import java.util.List;

public interface TaskService {
    List<Task> findAll();

    Task getById(Long id);

    Task updateTaskById(Long id, Task task);

    Task create(Task task);

    void deleteTaskById(Long id);
}
