package com.project.controller;

import com.project.repository.model.Task;
import com.project.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/")
public class TaskRestController {

    private final TaskService taskService;

    @Autowired
    public TaskRestController(TaskService taskService) {
        this.taskService = taskService;
    }

    @GetMapping(value = "tasks")
    public ResponseEntity<List<Task>> findAll() {
        List<Task> taskList = taskService.findAll();
        return new ResponseEntity<>(taskList, HttpStatus.OK);
    }

    @GetMapping(value = "tasks/{taskId}")
    public ResponseEntity<Task> findById(@PathVariable("taskId") Long taskId) {
        Task task = taskService.getById(taskId);
        return new ResponseEntity<>(task, HttpStatus.OK);
    }

    @PostMapping(value = "tasks")
    public ResponseEntity<Task> create(@RequestBody Task task) {
        Task newTask = taskService.create(task);
        return new ResponseEntity<>(newTask, HttpStatus.CREATED);
    }

    @PutMapping(value = "tasks/{taskId}")
    public ResponseEntity<Task> update(@RequestBody Task task, @PathVariable("taskId") Long taskId) {
        Task saveTask = taskService.updateTaskById(taskId, task);
        return new ResponseEntity<>(saveTask, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "tasks/{taskId}")
    public void deleteUser(@PathVariable("taskId") Long taskId) {
        taskService.deleteTaskById(taskId);
    }

}
