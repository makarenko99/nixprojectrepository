package com.project.controller.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ViewController {

    @RequestMapping(value = "/view-users")
    public String viewUsers() {
        return "view-users";
    }

    @RequestMapping(value = "/add-users")
    public String addUsers() {
        return "add-users";
    }

    @RequestMapping(value = "/view-tasks")
    public String viewTasks() {
        return "view-tasks";
    }

    @RequestMapping(value = "/add-tasks")
    public String addTasks(){
        return "add-tasks";
    }

    @RequestMapping(value = "/login")
    public String login(){
        return "login";
    }
}
