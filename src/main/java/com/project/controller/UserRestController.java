package com.project.controller;

import com.project.repository.model.User;
import com.project.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/")
public class UserRestController {

    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping(value = "users/{userId}")
    public ResponseEntity<User> findById(@PathVariable("userId") Long userId) {
        User user = userService.getById(userId);
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @GetMapping(value = "users")
    public ResponseEntity<List<User>> findAll() {
        List<User> findAll = userService.findAll();
        return new ResponseEntity<>(findAll, HttpStatus.OK);
    }

    @PostMapping(value = "users")
    public ResponseEntity<User> create(@RequestBody User user) {
        User newUser = userService.create(user);
        return new ResponseEntity<>(newUser, HttpStatus.CREATED);
    }

    @PutMapping(value = "users/{userId}")
    public ResponseEntity<User> updateUser(@RequestBody User user, @PathVariable("userId") Long id) {
        User saveUser = userService.updateUserById(id, user);
        return new ResponseEntity<>(saveUser, HttpStatus.ACCEPTED);
    }

    @DeleteMapping(value = "users/{userId}")
    public void deleteUser(@PathVariable("userId") Long userId) {
        userService.deleteUserById(userId);
    }
}
