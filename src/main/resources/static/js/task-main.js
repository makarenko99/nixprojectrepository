$(document).ready(function () {
    let tasks = {};
    let dynamicURL = "";
    let methodName = "";
    getAllTasks()
    $("#btnAddTask").click(function () {
        tasks.name = $("#taskName").val();
        let taskId = $("#id").val();
        if (taskId) {
            dynamicURL = "http://localhost:8080/api/v1/tasks/" + taskId;
            methodName = "PUT";
        } else {
            dynamicURL = "http://localhost:8080/api/v1/tasks/";
            methodName = "POST";
        }
        let taskJSON = JSON.stringify(tasks);
        $.ajax({
            url: dynamicURL,
            method: methodName,
            data: taskJSON,
            contentType: "application/json; charset=utf-8",
            success: function () {
                getAllTasks();
                location.reload();
            },
            error: function (error) {
                alert(error)
            }
        })
    })

    $("#loadTaskForm").click(function () {
        $("#resultReturnTasks").load("add-tasks");
    });

    $("#btnExit").click(function () {
        $("#resultReturnTasks").load("view-tasks");
    });
});

function getAllTasks() {
    $.ajax({
        url: "http://localhost:8080/api/v1/tasks",
        method: "GET",
        cache: false,
        crossDomain: true,
        dataType: "json",
        success: function (data) {
            let tableBody = $("#tblTasks tbody");
            tableBody.empty()
            $(data).each(function (index, element) {
                tableBody.append('<tr><td>' + element.id + '</td>' +
                    '<td><a onclick="update(' + element.id + ')">' + element.name + '</a></td>' +
                    '<td><button onclick = "deleteTask(' + element.id + ')">Delete</button></td></tr>');
            })
        },
        error: function (error) {
            alert(error);
        }
    });
}

function update(id) {
    $("#resultReturnTasks").load("add-tasks");
    $.ajax({
        url: "http://localhost:8080/api/v1/tasks/" + id,
        method: "GET",
        dataType: "json",
        success: function (data) {
            $("#taskName").val(data.name);
            $("#id").val(data.id);
            $("#btnAddTask").val("Update task")
            getAllTasks();
        },
        error: function (error) {
            alert(error);
        }
    })
}

function deleteTask(id) {
    $.ajax({
        url: "http://localhost:8080/api/v1/tasks/" + id,
        method: "DELETE",
        success: function () {
            alert("record has been deleted");
            getAllTasks();
        },
        error: function (error) {
            alert(error)
        }
    })
}

