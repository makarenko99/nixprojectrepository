$(document).ready(function () {
    let users = {};
    let dynamicURL = "";
    let methodName = "";
    getAllUsers()
    $("#btnAddUser").click(function () {
        users.username = $("#txtName").val();
        users.password = $("#txtPass").val();
        let userId = $("#id").val();
        if (userId) {
            dynamicURL = "http://localhost:8080/api/v1/users/" + userId;
            methodName = "PUT";
        } else {
            dynamicURL = "http://localhost:8080/api/v1/users";
            methodName = "POST";
        }
        let userJSON = JSON.stringify(users);
        $.ajax({
            url: dynamicURL,
            method: methodName,
            data: userJSON,
            contentType: "application/json; charset=utf-8",
            success: function () {
                getAllUsers();
                location.reload();
            },
            error: function (error) {
                alert(error)
            }
        })
    })

    $("#loadUserForm").click(function () {
        $("#resultReturn").load("add-users");
    });

    $("#btnExit").click(function () {
        $("#resultReturn").load("view-users");
    });
});

function getAllUsers() {
    $.ajax({
        url: "http://localhost:8080/api/v1/users",
        method: "GET",
        dataType: "json",
        success: function (data) {
            let tableBody = $("#tblUser tbody");
            tableBody.empty()
            $(data).each(function (index, element) {
                tableBody.append('<tr><td>' + element.id + '</td>' +
                    '<td><a onclick="update(' + element.id + ')">' + element.username + '</a></td>' +
                    '<td>' + element.password + '</td>' +
                    '<td><button onclick = "deleteUser(' + element.id + ')">Delete</button></td></tr>');
            })
        },
        error: function (error) {
            alert(error);
        }
    });
}

function update(id) {
    $("#resultReturn").load("add-users");
    $.ajax({
        url: "http://localhost:8080/api/v1/users/" + id,
        method: "GET",
        dataType: "json",
        success: function (data) {
            $("#txtName").val(data.username);
            $("#txtPass").val(data.password);
            $("#id").val(data.id);
            $("#btnAddUser").val("Update user")
            getAllUsers();
        },
        error: function (error) {
            alert(error);
        }
    })
}

function deleteUser(id) {
    $.ajax({
        url: "http://localhost:8080/api/v1/users/" + id,
        method: "DELETE",
        success: function () {
            alert("record has been deleted");
            getAllUsers();
        },
        error: function (error) {
            alert(error);
        }
    })
}

