$(function() {
    $("#btnLogin").click(function () {
        let username=$("#userName").val();
        let password=$("#password").val();
        $.ajax({
            type: "POST",
            url: "http://localhost:8080/api/v1/auth/login",
            contentType: "application/json;charset=UTF-8",
            data:JSON.stringify({"username":username ,"password" : password}),
            dataType: "json",
            async: false,
            error: function (request) {
                console.log("Connection error");
            },
            success: function (data) {
                $.cookie("token", data.token);
            }
        });
    });
});




