package com.project.service.impl;

import com.project.repository.TaskRepository;
import com.project.repository.model.Task;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@DataJpaTest
@ExtendWith(MockitoExtension.class)
public class TaskServiceImplTest {

    private final static String CREATE_REST_API = "create Rest api";
    private final static String CREATE_SERVICE = "create service";
    private final static String CREATE_REPOSITORY = "create repository";

    @InjectMocks
    private TaskServiceImpl taskService;

    @Mock
    private TaskRepository taskRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFindAllTasks() {
        List<Task> taskListMock = new ArrayList<>();

        Task taskOne = new Task();
        taskOne.setName(CREATE_REST_API);
        Task taskTwo = new Task();
        taskTwo.setName(CREATE_SERVICE);
        Task taskThree = new Task();
        taskThree.setName(CREATE_REPOSITORY);

        taskListMock.add(taskOne);
        taskListMock.add(taskTwo);
        taskListMock.add(taskThree);

        when(taskRepository.findAll()).thenReturn(taskListMock);

        List<Task> tasks = taskService.findAll();

        assertEquals(taskListMock.size(), tasks.size());

        verify(taskRepository, times(1)).findAll();
    }

    @Test
    public void shouldGetTaskById() {
        Task mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setName(CREATE_SERVICE);

        when(taskRepository.findById(mockTask.getId())).thenReturn(Optional.of(mockTask));

        Task newTask = taskService.getById(mockTask.getId());

        assertThat(mockTask.getId()).isEqualTo(newTask.getId());
        assertThat(mockTask.getName()).isEqualTo(newTask.getName());
        verify(taskRepository, times(1)).findById(mockTask.getId());
    }

    @Test
    public void shouldCreateTask() {
        Task mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setName(CREATE_REST_API);

        when(taskRepository.save(mockTask)).thenReturn(mockTask);

        Task newTask = taskService.create(mockTask);

        assertThat(mockTask.getId()).isEqualTo(newTask.getId());
        assertThat(mockTask.getName()).isEqualTo(newTask.getName());

        verify(taskRepository, times(1)).save(mockTask);
    }

    @Test
    public void shouldDeleteTaskById() {
        Long taskId = 1L;

        taskService.deleteTaskById(taskId);
        taskService.deleteTaskById(taskId);

        verify(taskRepository, times(2)).deleteById(taskId);
    }
}
