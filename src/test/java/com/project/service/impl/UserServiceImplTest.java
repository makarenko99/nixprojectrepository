package com.project.service.impl;

import com.project.repository.UserRepository;
import com.project.repository.model.User;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceImplTest {

    private static final String NAME_SERHII = "Serhii";
    private static final String NAME_IVAN = "Ivan";
    private static final String NAME_VASYA = "Vasya";

    @Autowired
    private UserServiceImpl userService;

    @MockBean
    private UserRepository userRepository;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void shouldFindAllUsers() {
        List<User> userListMock = new ArrayList<>();

        User userOne = new User();
        userOne.setUsername(NAME_SERHII);

        User userTwo = new User();
        userTwo.setUsername(NAME_VASYA);

        User userThree = new User();
        userThree.setUsername(NAME_IVAN);

        userListMock.add(userOne);
        userListMock.add(userTwo);
        userListMock.add(userThree);

        when(userRepository.findAll()).thenReturn(userListMock);

        List<User> users = userService.findAll();

        assertEquals(userListMock.size(), users.size());

        verify(userRepository, times(1)).findAll();
    }

    @Test
    public void shouldGetUserById() {
        User mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername(NAME_SERHII);

        when(userRepository.findById(mockUser.getId())).thenReturn(Optional.of(mockUser));

        User newUser = userService.getById(mockUser.getId());

        assertThat(mockUser.getId()).isEqualTo(newUser.getId());
        assertThat(mockUser.getUsername()).isEqualTo(newUser.getUsername());
        verify(userRepository, times(1)).findById(mockUser.getId());
    }


    @Test
    public void shouldCreateUser() {
        User mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername(NAME_SERHII);

        when(userRepository.save(mockUser)).thenReturn(mockUser);

        User newUser = userService.create(mockUser);

        assertThat(mockUser.getId()).isEqualTo(newUser.getId());
        assertThat(mockUser.getUsername()).isEqualTo(newUser.getUsername());

        verify(userRepository, times(1)).save(mockUser);
    }

    @Test
    public void shouldDeleteUserById() {
        Long userId = 1L;

        userService.deleteUserById(userId);
        userService.deleteUserById(userId);

        verify(userRepository, times(2)).deleteById(userId);
    }
}
