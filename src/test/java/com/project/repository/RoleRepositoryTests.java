package com.project.repository;

import com.project.repository.model.Role;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@DataJpaTest
public class RoleRepositoryTests {

    private static final String ROLE_MANAGER = "ROLE_MANAGER";
    private static final String ROLE_USER = "USER";
    private static final String ROLE_ADMIN = "ADMIN";
    private static final String COLUMN_NAME = "name";

    @Autowired
    private RoleRepository roleRepository;

    @Test
    public void shouldStoreRole() {
        Role role = new Role();
        role.setName(ROLE_USER);
        roleRepository.save(role);

        assertThat(role).hasFieldOrPropertyWithValue(COLUMN_NAME, ROLE_USER);
    }

    @Test
    public void shouldFindRoleByName() {
        Role roleUser = new Role();
        roleUser.setName(ROLE_USER);
        roleRepository.save(roleUser);

        Role roleAdmin = new Role();
        roleAdmin.setName(ROLE_ADMIN);
        roleRepository.save(roleAdmin);

        Role roleFound = roleRepository.findByName(roleAdmin.getName());

        assertThat(roleFound).isEqualTo(roleAdmin);
    }


    @Test
    public void shouldUpdateRoleByName() {

        Role roleUser = new Role();
        roleUser.setName(ROLE_USER);
        roleRepository.save(roleUser);

        Role roleAdmin = new Role();
        roleUser.setName(ROLE_ADMIN);
        roleRepository.save(roleAdmin);

        Role updateRole = new Role();
        updateRole.setName(ROLE_MANAGER);

        Role role = roleRepository.findByName(roleAdmin.getName());
        role.setName(updateRole.getName());
        roleRepository.save(role);

        Role checkRole = roleRepository.findByName(roleAdmin.getName());

        assertThat(checkRole.getId()).isEqualTo(roleAdmin.getId());
        assertThat(checkRole.getName()).isEqualTo(updateRole.getName());
    }

    @Test
    public void shouldDeleteRoleById() {
        Role roleUser = new Role();
        roleUser.setName(ROLE_USER);
        roleRepository.save(roleUser);

        Role roleAdmin = new Role();
        roleAdmin.setName(ROLE_ADMIN);
        roleRepository.save(roleAdmin);

        roleRepository.deleteById(roleAdmin.getId());

        List<Role> roleList = roleRepository.findAll();

        assertThat(roleList).hasSize(1).contains(roleUser);
    }

}
