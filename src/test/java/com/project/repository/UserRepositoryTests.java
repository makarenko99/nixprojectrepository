package com.project.repository;

import com.project.repository.model.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;


@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTests {

    private static final String NAME_SERHII = "Serhii";
    private static final String NAME_IVAN = "Ivan";
    private static final String NAME_VASYA = "Vasya";
    private static final String COLUMN_NAME = "name";

    @Autowired
    private UserRepository userRepository;

    @Test
    public void shouldStoreUser() {
        User user1 = new User();
        user1.setUsername(NAME_SERHII);
        userRepository.save(user1);

        assertThat(user1).hasFieldOrPropertyWithValue(COLUMN_NAME, NAME_SERHII);
    }

    @Test
    public void shouldFindUserById() {
        User user1 = new User();
        user1.setUsername(NAME_SERHII);
        userRepository.save(user1);

        User user2 = new User();
        user2.setUsername(NAME_VASYA);
        userRepository.save(user2);

        User foundUser = userRepository.findById(user2.getId()).get();

        assertThat(foundUser).isEqualTo(user2);

    }

    @Test
    public void shouldUpdateUserById() {
        User user1 = new User();
        user1.setUsername(NAME_SERHII);
        userRepository.save(user1);

        User user2 = new User();
        user2.setUsername(NAME_IVAN);
        userRepository.save(user2);

        User updateUser = new User();
        updateUser.setUsername(NAME_VASYA);

        User user = userRepository.findById(user2.getId()).get();
        user.setUsername(updateUser.getUsername());
        userRepository.save(user);

        User checkUser = userRepository.findById(user2.getId()).get();

        assertThat(checkUser.getId()).isEqualTo(user2.getId());
        assertThat(checkUser.getUsername()).isEqualTo(updateUser.getUsername());

    }


    @Test
    public void shouldDeleteUserById() {
        User user1 = new User();
        user1.setUsername(NAME_SERHII);
        userRepository.save(user1);

        User user2 = new User();
        user2.setUsername(NAME_IVAN);
        userRepository.save(user2);

        User user3 = new User();
        user3.setUsername(NAME_VASYA);
        userRepository.save(user3);

        userRepository.deleteById(user2.getId());

        List<User> userList = userRepository.findAll();

        assertThat(userList).hasSize(2).contains(user1, user3);
    }

}
