package com.project.repository;

import static org.assertj.core.api.Assertions.assertThat;

import com.project.repository.model.Task;
import com.project.repository.model.User;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class TaskRepositoryTests {

    private static final String NAME_SERHII = "Serhii";
    private final static String MAKE_HTML_SITE = "make html site";
    private final static String CREATE_REST_API = "create Rest api";
    private static final String NAME_VASYA = "Vasya";
    private static final String COLUMN_NAME = "name";

    @Autowired
    private TaskRepository taskRepository;

    @Test
    public void shouldStoreTask() {
        User user = new User();
        user.setUsername(NAME_SERHII);
        Task task = new Task();
        task.setName(MAKE_HTML_SITE);
        task.setUser(user);
        taskRepository.save(task);
        assertThat(task).hasFieldOrPropertyWithValue(COLUMN_NAME, MAKE_HTML_SITE);
    }

    @Test
    public void shouldFindTaskById() {
        User user = new User();
        user.setUsername(NAME_SERHII);
        Task task1 = new Task();
        task1.setName(MAKE_HTML_SITE);
        task1.setUser(user);
        taskRepository.save(task1);

        Task task2 = new Task();
        task2.setName(MAKE_HTML_SITE);
        task2.setUser(user);
        taskRepository.save(task2);

        Task foundTask = taskRepository.findById(task2.getId()).get();

        assertThat(foundTask).isEqualTo(task2);
    }

    @Test
    public void shouldUpdateTaskById() {
        User user = new User();
        user.setUsername(NAME_SERHII);
        Task task1 = new Task();
        task1.setName(MAKE_HTML_SITE);
        task1.setUser(user);
        taskRepository.save(task1);

        Task task2 = new Task();
        task2.setName(CREATE_REST_API);
        task2.setUser(user);
        taskRepository.save(task2);

        User user1 = new User();
        user.setUsername(NAME_VASYA);
        Task updateTask = new Task();
        updateTask.setName(MAKE_HTML_SITE);
        updateTask.setUser(user1);

        Task task = taskRepository.findById(task2.getId()).get();
        task.setName(updateTask.getName());
        task.setUser(updateTask.getUser());
        taskRepository.save(task);

        Task checkTask = taskRepository.findById(task2.getId()).get();

        assertThat(checkTask.getId()).isEqualTo(task2.getId());
        assertThat(checkTask.getName()).isEqualTo(updateTask.getName());
        assertThat(checkTask.getUser()).isEqualTo(updateTask.getUser());
    }

    @Test
    public void shouldDeleteTaskById() {
        User user = new User();
        user.setUsername(NAME_SERHII);
        Task task1 = new Task();
        task1.setName(MAKE_HTML_SITE);
        task1.setUser(user);
        taskRepository.save(task1);

        Task task2 = new Task();
        task2.setName(CREATE_REST_API);
        task2.setUser(user);
        taskRepository.save(task2);

        taskRepository.deleteById(task2.getId());

        List<Task> taskList = taskRepository.findAll();

        assertThat(taskList).hasSize(1).contains(task1);
    }
}
