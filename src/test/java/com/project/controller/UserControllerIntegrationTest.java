package com.project.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.repository.model.User;
import com.project.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private UserService userService;

    @Test
    public void shouldGetAllUsers() throws Exception {

        List<User> userListMock = new ArrayList<>();
        User user1 = new User();
        user1.setId(1L);
        user1.setUsername("admin");
        user1.setPassword("admin");
        User user2 = new User();
        user2.setId(1L);
        user2.setUsername("user");
        user2.setPassword("user");
        userListMock.add(user1);
        userListMock.add(user2);

        Mockito.when(userService.findAll()).thenReturn(userListMock);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/users/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].username", is("admin")))
                .andExpect(jsonPath("$[0].password", is("admin")))
                .andExpect(jsonPath("$[1].username", is("user")))
                .andExpect(jsonPath("$[1].password", is("user")));
    }

    @Test
    public void shouldCreateUser() throws Exception {
        User mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("admin");
        mockUser.setPassword("admin");

        Mockito.when(userService.create(Mockito.any(User.class))).thenReturn(mockUser);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/v1/users")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(mockUser));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.username", is("admin")))
                .andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(mockUser)));
    }

    @Test
    public void shouldUpdatedUser() throws Exception {
        User mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("admin");
        mockUser.setPassword("admin");

        Mockito.when(userService.updateUserById(mockUser.getId(), mockUser)).thenReturn(mockUser);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put("/api/v1/users/1", mockUser).contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON).characterEncoding("UTF-8")
                .content(this.mapper.writeValueAsBytes(mockUser));

        mockMvc.perform(builder).andExpect(status().isAccepted())
                .andExpect(jsonPath("$.username", is("admin")))
                .andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(mockUser)));
    }

    @Test
    public void shouldDeleteUser() throws Exception {
        User mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUsername("admin");
        mockUser.setPassword("admin");

        UserService serviceSpy = Mockito.spy(userService);
        Mockito.doNothing().when(serviceSpy).deleteUserById(mockUser.getId());

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/users/1")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verify(userService, times(1)).deleteUserById(mockUser.getId());
    }
}
