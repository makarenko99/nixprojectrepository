package com.project.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.repository.TaskRepository;
import com.project.repository.model.Task;
import com.project.repository.model.User;
import com.project.service.TaskService;
import com.project.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class TaskControllerIntegrationTest {


    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private TaskService taskService;

    @Test
    public void shouldGetAllUsers() throws Exception {

        List<Task> taskListMock = new ArrayList<>();
        Task task1 = new Task();
        task1.setId(1L);
        task1.setName("task 1");
        Task task2 = new Task();
        task2.setId(2L);
        task2.setName("task 2");
        taskListMock.add(task1);
        taskListMock.add(task2);

        Mockito.when(taskService.findAll()).thenReturn(taskListMock);

        mockMvc.perform(MockMvcRequestBuilders.get("/api/v1/tasks/").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk()).andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].name", is("task 1")))
                .andExpect(jsonPath("$[1].name", is("task 2")));
    }

    @Test
    public void shouldCreateUser() throws Exception {
        Task mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setName("task 1");
        Mockito.when(taskService.create(Mockito.any(Task.class))).thenReturn(mockTask);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders.post("/api/v1/tasks")
                .contentType(MediaType.APPLICATION_JSON_VALUE).accept(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF-8").content(this.mapper.writeValueAsBytes(mockTask));

        mockMvc.perform(builder)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.name", is("task 1")))
                .andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(mockTask)));
    }

    @Test
    public void shouldUpdatedTask() throws Exception {
        Task mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setName("task 1");

        Mockito.when(taskService.updateTaskById(mockTask.getId(), mockTask)).thenReturn(mockTask);

        MockHttpServletRequestBuilder builder = MockMvcRequestBuilders
                .put("/api/v1/tasks/1", mockTask).contentType(MediaType.APPLICATION_JSON_VALUE)
                .accept(MediaType.APPLICATION_JSON).characterEncoding("UTF-8")
                .content(this.mapper.writeValueAsBytes(mockTask));

        mockMvc.perform(builder).andExpect(status().isAccepted())
                .andExpect(jsonPath("$.name", is("task 1")))
                .andExpect(MockMvcResultMatchers.content().string(this.mapper.writeValueAsString(mockTask)));
    }

    @Test
    public void shouldDeleteTask() throws Exception {
        Task mockTask = new Task();
        mockTask.setId(1L);
        mockTask.setName("task 1");

        TaskService serviceSpy = Mockito.spy(taskService);
        Mockito.doNothing().when(serviceSpy).deleteTaskById(mockTask.getId());

        mockMvc.perform(MockMvcRequestBuilders.delete("/api/v1/tasks/1")
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk());

        verify(taskService, times(1)).deleteTaskById(mockTask.getId());
    }

}
